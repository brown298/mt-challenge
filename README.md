#MT Code Challenge

The Problem:

Given the input file provided, generate output that looks like:

    <Host>: Average: <average value>  Max: <maxium value> Min: <minimum value>

Ordered by Average value, largest to smallest.

Example:

    n10: Average: 55.0 Max: 85.2 Min: 12.1

The host name is the first field in each row of the file.

#Installation
Installing this application requires composer (see https://getcomposer.org/download/). Once you have composer installed:
1. Run composer install: `php composer.phar install --dev`

# Testing
PHPUnit is the testing framework of choice.
1. Run `bin/phpunit` (add `--debug` flag to see individual tests).

#Execution
Help screen:

    $ bin/console --help
    Usage:
      mt:code_challenge [options] [--] <input_file>
    
    Arguments:
      input_file                                   path to the file to be read
    
    Options:
      -d, --directory[=DIRECTORY]                  directory to search for files in [default: "/var/www/extensiblephp/mt"]
          --sorting_direction[=SORTING_DIRECTION]  what direction to sort the values by (asc, desc) [default: "asc"]
          --sorting_type[=SORTING_TYPE]            what attribute to sort the values by (average, min, max) [default: "average"]
          --include_none                           Indicates if "None" should be treated as a 0
      -h, --help                                   Display this help message
      -q, --quiet                                  Do not output any message
      -V, --version                                Display this application version
          --ansi                                   Force ANSI output
          --no-ansi                                Disable ANSI output
      -n, --no-interaction                         Do not ask any interactive question
      -v|vv|vvv, --verbose                         Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

Sorting ascending by average:

    $ bin/console "Coding Demo Data.txt"
    
Sorting descending by average:

    $ bin/console --sorting_direction=desc  "Coding Demo Data.txt"
    
Sorting descending by min:

    $ bin/console --sorting_direction=desc --sorting_type=min "Coding Demo Data.txt"
    
Sorting asc by max:

    $ bin/console --sorting_direction=asc --sorting_type=max "Coding Demo Data.txt"
    
Verbose output:

    $ bin/console "Coding Demo Data.txt" -vvv

If you would want to consider "None" to be 0:

    $ bin/console "Coding Demo Data.txt" --include_none


