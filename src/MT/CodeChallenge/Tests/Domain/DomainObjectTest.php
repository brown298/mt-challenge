<?php
/**
 * User: John Brown
 * Date: 8/7/2017
 */
namespace MT\CodeChallenge\Tests\Domain;

use MT\CodeChallenge\Domain\DomainObject;
use PHPUnit\Framework\TestCase;
use Phake;
use Psr\Log\LoggerInterface;

/**
 * Class DomainObjectTest
 * @package MT
 * @subpackage CodeChallenge\Tests\Domain
 */
class DomainObjectTest extends TestCase
{

    /**
     * @var \MT\CodeChallenge\Domain\DomainObjectInterface
     */
    protected $domainObject;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->domainObject = new DomainObject();
    }

    /**
     * @return array
     */
    public function findMaxProvider()
    {
        return [
            [ [1,12,20,8,30,50,90,45], 90 ],
            [ [1,12,20,8,30,50,18,45], 50 ],
        ];
    }

    /**
     * @param array $inputArray
     * @param $expectedResult
     * @dataProvider findMaxProvider
     */
    public function testFindMax(array $inputArray, $expectedResult)
    {
        $this->domainObject->setData($inputArray);
        $result = $this->domainObject->findMax();
        $this->assertEquals($expectedResult, $result);
    }
    /**
     * @return array
     */
    public function findMinProvider()
    {
        return [
            [ [1,12,20,8,30,50,90,45], 1 ],
            [ [90,12,20,8,30,50,18,45], 8 ],
        ];
    }

    /**
     * @param array $inputArray
     * @param $expectedResult
     * @dataProvider findMinProvider
     */
    public function testFindMin(array $inputArray, $expectedResult)
    {
        $this->domainObject->setData($inputArray);
        $result = $this->domainObject->findMin();
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @return array
     */
    public function calculateAverageProvider()
    {
        return [
            [ [1,12,20,8,30,50,90,45], 32 ],
            [ [90,12,20,8,30,50,18,45], 34.125 ],
        ];
    }

    /**
     * @param array $inputArray
     * @param $expectedResult
     * @dataProvider calculateAverageProvider
     */
    public function testCalculateAverage(array $inputArray, $expectedResult)
    {
        $this->domainObject->setData($inputArray);
        $result = $this->domainObject->calculateAverage();
        $this->assertEquals($expectedResult, $result);
    }
}