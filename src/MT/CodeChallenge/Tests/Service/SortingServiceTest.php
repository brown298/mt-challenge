<?php
/**
 * User: John Brown
 * Date: 8/7/2017
 */
namespace MT\CodeChallenge\Tests\Service;

use MT\CodeChallenge\Domain\DomainObjectInterface;
use MT\CodeChallenge\Service\SortingService;
use Phake;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * Class SortingServiceTest
 * @package MT
 * @subpackage CodeChallenge\Tests\Service
 */
class SortingServiceTest extends TestCase
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \MT\CodeChallenge\Domain\DomainObjectInterface
     */
    protected $domainObject;

    /**
     * @var SortingService
     */
    protected $service;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->logger = Phake::mock('\Psr\Log\LoggerInterface');
        $this->domainObject = Phake::mock('\MT\CodeChallenge\Domain\DomainObjectInterface');
        $this->service = Phake::partialMock('\MT\CodeChallenge\Service\SortingService', $this->logger);
    }

    /**
     * provides test values for sorting
     *
     * @return array
     */
    public function sortingComparisonProvider()
    {
        return [
            [1, 2, SortingService::SORT_TYPE_AVERAGE, SortingService::SORT_DIR_ASC, -1],
            [2, 1, SortingService::SORT_TYPE_AVERAGE, SortingService::SORT_DIR_ASC, 1],
            [2, 2, SortingService::SORT_TYPE_AVERAGE, SortingService::SORT_DIR_ASC, 0],
            [1, 2, SortingService::SORT_TYPE_AVERAGE, SortingService::SORT_DIR_DESC, 1],
            [2, 1, SortingService::SORT_TYPE_AVERAGE, SortingService::SORT_DIR_DESC, -1],
            [2, 2, SortingService::SORT_TYPE_AVERAGE, SortingService::SORT_DIR_DESC, 0],
            [1, 2, SortingService::SORT_TYPE_MIN, SortingService::SORT_DIR_ASC, -1],
            [2, 1, SortingService::SORT_TYPE_MIN, SortingService::SORT_DIR_ASC, 1],
            [2, 2, SortingService::SORT_TYPE_MIN, SortingService::SORT_DIR_ASC, 0],
            [1, 2, SortingService::SORT_TYPE_MIN, SortingService::SORT_DIR_DESC, 1],
            [2, 1, SortingService::SORT_TYPE_MIN, SortingService::SORT_DIR_DESC, -1],
            [2, 2, SortingService::SORT_TYPE_MIN, SortingService::SORT_DIR_DESC, 0],
            [1, 2, SortingService::SORT_TYPE_MAX, SortingService::SORT_DIR_ASC, -1],
            [2, 1, SortingService::SORT_TYPE_MAX, SortingService::SORT_DIR_ASC, 1],
            [2, 2, SortingService::SORT_TYPE_MAX, SortingService::SORT_DIR_ASC, 0],
            [1, 2, SortingService::SORT_TYPE_MAX, SortingService::SORT_DIR_DESC, 1],
            [2, 1, SortingService::SORT_TYPE_MAX, SortingService::SORT_DIR_DESC, -1],
            [2, 2, SortingService::SORT_TYPE_MAX, SortingService::SORT_DIR_DESC, 0],
        ];
    }

    /**
     * @param $valueA
     * @param $valueB
     * @param $type
     * @param $direction
     * @param $expectedResult
     * @dataProvider sortingComparisonProvider
     */
    public function testSortingComparison($valueA, $valueB, $type, $direction, $expectedResult)
    {
        $this->service->setSortType($type);
        $this->service->setSortDirection($direction);
        $objectB = Phake::mock('\MT\CodeChallenge\Domain\DomainObjectInterface');
        $objectA = Phake::mock('\MT\CodeChallenge\Domain\DomainObjectInterface');

        Phake::when($objectA)->findMin()->thenReturn($valueA);
        Phake::when($objectA)->findMax()->thenReturn($valueA);
        Phake::when($objectA)->calculateAverage()->thenReturn($valueA);

        Phake::when($objectB)->findMin()->thenReturn($valueB);
        Phake::when($objectB)->findMax()->thenReturn($valueB);
        Phake::when($objectB)->calculateAverage()->thenReturn($valueB);

        $result = $this->service->sortComparison($objectA, $objectB);

        $this->assertEquals($expectedResult, $result, 'invalid sorting order returned');
    }

    /**
     * provides data to test the sort function
     *
     * @return array
     */
    public function sortDataProvider()
    {
        return [
            [
                'asc',
                [37,26,65,12,100],
                [12,26,37,65,100],
            ],
            [
                'desc',
                [37,26,65,12,100],
                [100,65,37,26,12],
            ],
        ];
    }

    /**
     * testSort
     *
     * ensures sorted arrays come out sorted correctly
     * @param $direction
     * @param array $data
     * @param array $expectedResult
     * @dataProvider sortDataProvider
     */
    public function testSort($direction, array $data, array $expectedResult)
    {
        $domainObjects = [];
        foreach ($data as $value) {
            $obj = Phake::mock('\MT\CodeChallenge\Domain\DomainObjectInterface');
            Phake::when($obj)->calculateAverage()->thenReturn($value);
            $domainObjects[] = $obj;
        }

        $this->service->setDomainObjects($domainObjects);

        $result = $this->service->sort($direction)->getDomainObjects();
        foreach ($result as $i => $v ) {
            $this->assertEquals($expectedResult[$i], $v->calculateAverage(), 'sort failed');
        }
    }
}