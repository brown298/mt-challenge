<?php
/**
 * User: John Brown
 * Date: 8/7/2017
 */
namespace MT\CodeChallenge\Tests\Service;

use MT\CodeChallenge\Domain\DomainObjectInterface;
use MT\CodeChallenge\Service\DomainParserService;
use MT\CodeChallenge\Service\InputParserService;
use PHPUnit\Framework\TestCase;
use Phake;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Finder\Finder;

/**
 * Class InputParserServiceTest
 * @package MT
 * @subpackage CodeChallenge\Tests\Service
 */
class InputParserServiceTest extends TestCase
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var Finder
     */
    protected $finder;

    /**
     * @var DomainParserService
     */
    protected $domainParser;

    /**
     * @var DomainObjectInterface
     */
    protected $domainObject;

    /**
     * @var Symfony\Component\Finder\SplFileInfo
     */
    protected $fileInfo;

    /**
     * @var InputParserService
     */
    protected $service;

    public function setUp()
    {
        $this->logger = Phake::mock('\Psr\Log\LoggerInterface');
        $this->finder = Phake::mock('Symfony\Component\Finder\Finder');
        $this->domainParser = Phake::mock('MT\CodeChallenge\Service\DomainParserService');
        $this->fileInfo = Phake::mock('Symfony\Component\Finder\SplFileInfo');
        $this->domainObject = Phake::mock('\MT\CodeChallenge\Domain\DomainObjectInterface');

        Phake::when($this->finder)->name(Phake::anyParameters())->thenReturn($this->finder);
        Phake::when($this->finder)->in(Phake::anyParameters())->thenReturn($this->finder);
        Phake::when($this->domainParser)->parseLine(Phake::anyParameters())->thenReturn($this->domainObject);

        $this->service = new InputParserService($this->finder, $this->domainParser, $this->logger);
    }

    /**
     * ensure we throw an exception when the file isn't found
     *
     * @expectedException \Symfony\Component\Filesystem\Exception\FileNotFoundException
     */
    public function testLoadFileThrowsExceptionOnNotFound()
    {
        $this->service->loadFile('/tmp', 'nonExistantFile.txt');
    }

    /**
     * @expectedException \MT\CodeChallenge\Exception\FileOpenException
     */
    public function testLoadFileThrowsExeptionOnInvalidFile()
    {
        $this->service = Phake::partialMock('\MT\CodeChallenge\Service\InputParserService', $this->finder, $this->domainParser, $this->logger);
        $iterator = new \ArrayIterator();
        $iterator->append($this->fileInfo);
        Phake::when($this->finder)->in(Phake::anyParameters())->thenReturn($iterator);
        Phake::when($this->fileInfo)->getFilename()->thenReturn('nonExistantFile.txt');
        Phake::when($this->service)->getFileHandle(Phake::anyParameters())->thenReturn(false);

        $this->service->loadFile('/tmp', 'nonExistantFile.txt');
    }


    /**
     * ensure we create domain objects when we have a valid file
     */
    public function testLoadFileParsesFileCreatesDomainObjects()
    {
        $finder = new Finder();
        $this->service = new InputParserService($finder, $this->domainParser, $this->logger);

        $result = $this->service->loadFile(__DIR__ . '/../', 'example_input.txt');

        Phake::verify($this->domainParser, Phake::atLeast(9))->parseLine(Phake::anyParameters());
        $this->assertEmpty($result); // we expect an empty array since no data was in the domain objects
    }
}