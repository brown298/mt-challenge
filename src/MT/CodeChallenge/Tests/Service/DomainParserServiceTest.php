<?php
/**
 * User: John Brown
 * Date: 8/7/2017
 */
namespace MT\CodeChallenge\Tests\Service;

use MT\CodeChallenge\Service\DomainParserService;
use PHPUnit\Framework\TestCase;
use Phake;

/**
 * Class DomainParserServiceTest
 * @package MT
 * @subpackage CodeChallenge\Tests\Service
 */
class DomainParserServiceTest extends TestCase
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var DomainParserService
     */
    protected $service;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->logger = Phake::mock('\Psr\Log\LoggerInterface');
        $this->service = Phake::partialMock('\MT\CodeChallenge\Service\DomainParserService', $this->logger);
    }

    /**
     * ensure the validator works on a valid value
     */
    public function testValidateLineValid()
    {
        $line = "n30,1366829460,1366831260,60|100.0,94.0,100.0,100.0,100.0,99.0,88.0,76.0,99.0,75.0,60.0,70.0,85.0,88.0,99.0,70.0,97.0,84.0,91.0,98.0,80.0,82.0,44.0,79.0,92.0,96.0,77.0,88.0,99.0,65.0";
        $this->assertEquals(1, $this->service->validateLine($line));
    }

    /**
     * @return array
     */
    public function invalidDataProvider()
    {
        return [
            ["n30,13668294601366831260,60|100.0,94.0,100.0,100.0,100.0,99.0,88.0,76.0,99.0,75.0,60.0,70.0,85.0,88.0,99.0,70.0,97.0,84.0,91.0,98.0,80.0,82.0,44.0,79.0,92.0,96.0,77.0,88.0"],
            ["n30,1366829460,1366831260,60100.0,94.0,100.0,100.0,100.0,99.0,88.0,76.0,99.0,75.0,60.0,70.0,85.0,88.0,99.0,70.0,97.0,84.0,91.0,98.0,80.0,82.0,44.0,79.0,92.0,96.0,77.0,88.0"],
            ["n30,1366829460,1366831260,60|"],
        ];
    }

    /**
     * ensure the validator works on a valid value
     * @param string $invalidLine
     * @dataProvider invalidDataProvider
     */
    public function testValidateLineTooInvalidFormat(string $invalidLine)
    {
        $this->assertEquals(0, $this->service->validateLine($invalidLine));
    }

    /**
     * testParseLineProduceCorrectDomainObject
     */
    public function testParseLineProduceCorrectDomainObject()
    {
        $line = "n30,1366829460,1366831260,60|100.0,94.0,100.0,100.0,100.0,99.0,88.0,76.0,99.0,75.0,60.0,70.0,85.0,88.0,99.0,70.0,97.0,84.0,91.0,98.0,80.0,82.0,44.0,79.0,92.0,96.0,77.0,88.0,99.0,65.0";
        $result = $this->service->parseLine(1, $line);

        $this->assertEquals('n30', $result->getHost());
        $this->assertEquals('2013-04-24 18:51:00', $result->getStartTime()->format('Y-m-d H:i:s'));
        $this->assertEquals('2013-04-24 19:21:00', $result->getEndTime()->format('Y-m-d H:i:s'));
        $this->assertEquals([100.0,94.0,100.0,100.0,100.0,99.0,88.0,76.0,99.0,75.0,60.0,70.0,85.0,88.0,99.0,70.0,97.0,84.0,91.0,98.0,80.0,82.0,44.0,79.0,92.0,96.0,77.0,88.0,99.0,65.0], $result->getData());
    }

    /**
     * testParseLineWithNoneIncluded
     * ensure we convert none to be a zero
     */
    public function testParseLineWithNoneIncluded()
    {
        $line = "n30,1366829460,1366831260,60|None,None,100.0,100.0,100.0,99.0,88.0,76.0,99.0,75.0,60.0,70.0,85.0,88.0,99.0,70.0,97.0,84.0,91.0,98.0,80.0,82.0,44.0,79.0,92.0,96.0,77.0,88.0,99.0,65.0";
        $result = $this->service->parseLine(1, $line, true);

        $this->assertEquals('n30', $result->getHost());
        $this->assertEquals([0,0,100.0,100.0,100.0,99.0,88.0,76.0,99.0,75.0,60.0,70.0,85.0,88.0,99.0,70.0,97.0,84.0,91.0,98.0,80.0,82.0,44.0,79.0,92.0,96.0,77.0,88.0,99.0,65.0], $result->getData());
    }
}
