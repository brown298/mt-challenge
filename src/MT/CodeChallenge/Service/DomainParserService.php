<?php
/**
 * User: John Brown
 * Date: 8/7/2017
 */

namespace MT\CodeChallenge\Service;

use MT\CodeChallenge\Domain\DomainObject;
use MT\CodeChallenge\Domain\DomainObjectInterface;
use MT\CodeChallenge\Exception\InvalidInputException;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;

/**
 * Class DomainParserService
 * @package MT
 * @subpackage CodeChallenge\Service
 */
class DomainParserService implements DomainParserInterface
{
    use LoggerAwareTrait;

    const NONE = 'None';
    const INPUT_VALIDATOR = '/^n[0-9]+,[0-9]+,[0-9]+,[0-9]+\|(?:None|(?:\d+|\d*\.\d+)|\,)+/';

    /**
     * DomainParserService constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->setLogger($logger);
    }

    /**
     * parseLine
     *
     * @param int    $lineNumber
     * @param string $line
     * @param bool   $includeNone
     * @return DomainObjectInterface
     * @throws InvalidInputException
     */
    public function parseLine(int $lineNumber, string $line, bool $includeNone = false): DomainObjectInterface
    {
        $this->logger->info(sprintf('parsing line %d; %s', $lineNumber, $line));
        if (!$this->validateLine($line)) {
            throw new InvalidInputException(sprintf('Invalid input at line %d: %s', $lineNumber, $line));
        }
        $domainObject = new DomainObject();
        list($header, $values) = explode('|', $line);
        list($host, $start, $end, $timeframe) = explode(',', $header);
        $data = explode(',', $values);

        $domainObject->setHost($host)
            ->setStartTime( (new \DateTime())->setTimestamp($start))
            ->setEndTime( (new \DateTime())->setTimestamp($end))
            ->setTimeframe(intval($timeframe))
            ->setData($this->parseData($data, $includeNone))
        ;

        return $domainObject;
    }

    /**
     * parseData
     *
     * @param mixed[] $data
     * @param bool    $includeNone determin if "None" is a 0 or null
     * @return float[]
     */
    private function parseData(array $data, bool $includeNone = false): array
    {
        $result = [];
        foreach ($data as $value) {
            switch ($value) {
                case self::NONE:
                    if ($includeNone) $result[] = 0;
                    break;
                default:
                    $result[] = floatval($value);
            }
        }
        return $result;
    }

    /**
     * @param string $line
     * @return mixed
     */
    public function validateLine(string $line)
    {
        return preg_match(self::INPUT_VALIDATOR, $line);
    }
}
