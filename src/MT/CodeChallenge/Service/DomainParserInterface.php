<?php
/**
 * User: John Brown
 * Date: 8/7/2017
 */
namespace MT\CodeChallenge\Service;

use MT\CodeChallenge\Domain\DomainObjectInterface;
use MT\CodeChallenge\Exception\InvalidInputException;

/**
 * Interface DomainParserInterface
 * @package MT
 * @subpackage CodeChallenge\Service
 */
interface DomainParserInterface
{
    /**
     * parseLine
     *
     * parses an input line
     *
     * @param int $lineNumber
     * @param string $line
     * @return mixed
     * @throws InvalidInputException
     */
    public function parseLine(int $lineNumber, string $line):DomainObjectInterface;

    /**
     * @param string $line
     * @return mixed
     */
    public function validateLine(string $line);
}