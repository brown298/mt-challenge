<?php
/**
 * User: John Brown
 * Date: 8/7/2017
 */
namespace MT\CodeChallenge\Service;

use MT\CodeChallenge\Domain\DomainObjectInterface;
use MT\CodeChallenge\Exception\FileOpenException;
use MT\CodeChallenge\Exception\InvalidInputException;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Class InputParserService
 * @package MT
 * @subpackage CodeChallenge\Service
 */
class InputParserService
{
    use LoggerAwareTrait;

    /**
     * @var Finder
     */
    private $finder;

    /**
     * @var bool determin if "None" is treated as 0 or null
     */
    private $includeNone = false;

    /**
     * InputParserService constructor.
     * @param Finder                $finder
     * @param DomainParserInterface $domainParser
     * @param LoggerInterface       $logger
     */
    public function __construct(Finder $finder, DomainParserInterface $domainParser, LoggerInterface $logger)
    {
        $this->finder = $finder;
        $this->setLogger($logger);
        $this->domainParser = $domainParser;
    }

    /**
     * includeNone
     * @param bool $include
     * return InputParserService
     */
    public function includeNone(bool $include=true):InputParserService
    {
        $this->includeNone = $include;
        return $this;
    }

    /**
     * @param string $baseDir
     * @param string $fileName
     * @return Finder
     */
    private function getFiles(string $baseDir, string $fileName)
    {
        $files = $this->finder->name($fileName)->in($baseDir);
        return $files;
    }

    /**
     * loadFile
     *
     * @param string $baseDir
     * @param string $fileName
     * @return DomainObjectInterface[]
     * @throws FileNotFoundException, FileOpenException
     */
    public function loadFile(string $baseDir, string $fileName)
    {
        $domainObjects = [];
        $files = $this->getFiles($baseDir, $fileName);
        if ($files->count() == 0) throw new FileNotFoundException(sprintf('Could not locate file %s', $fileName)); // could not find any files

        foreach ($files as $file) {
            $this->logger->debug(sprintf('loading file "%s" for reading', $file->getFilename()));
            $newDomainObjects = $this->readFile($file);
            $domainObjects = array_merge($domainObjects, $newDomainObjects);
        }

        return $domainObjects;
    }

    /**
     * stub to allow mocking around fopen
     * @param string $filename
     * @param string $mode
     * @return bool|resource
     */
    protected function getFileHandle(string $filename, string $mode)
    {
       return fopen($filename, $mode);
    }

    /**
     * readFile
     *
     * reads the file and builds domain objects out of the results
     *
     * @param SplFileInfo $file
     * @return mixed|DomainObjectInterface|null
     * @throws FileOpenException
     */
    private function readFile(SplFileInfo $file)
    {
        $domainObjects = [];
        $handle = $this->getFileHandle($file->getPath() . DIRECTORY_SEPARATOR . $file->getFilename(), "r");
        if (!$handle) throw new FileOpenException(sprintf('Could not open file %s', $file->getFilename())); // could not open file

        $count = 0;
        while (!feof($handle)) {
            $line = fgets($handle);
            if (!$line) break;

            try {
                $domainObject = $this->domainParser->parseLine($count, $line, $this->includeNone);

                if (count($domainObject->getData()) > 0) {
                    $domainObjects[] = $domainObject;
                }
            } catch (InvalidInputException $ex) { // log and more onto next line
                $this->logger->error($ex->getMessage());
            }

            $count++;
        }

        fclose($handle);
        return $domainObjects;
    }
}
