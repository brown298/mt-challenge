<?php
/**
 * User: John Brown
 * Date: 8/7/2017
 */

namespace MT\CodeChallenge\Service;

use MT\CodeChallenge\Domain\DomainObjectInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;

/**
 * Class SortingService
 * @package MT
 * @subpackage CodeChallenge\Service
 */
class SortingService
{
    use LoggerAwareTrait;

    CONST SORT_DIR_ASC = 'asc';
    CONST SORT_DIR_DESC = 'desc';
    CONST SORT_TYPE_MIN = 'min';
    CONST SORT_TYPE_MAX = 'max';
    CONST SORT_TYPE_AVERAGE = 'average';

    /**
     * @var DomainObjectInterface[]
     */
    private $domainObjects;

    /**
     * @var string type of information to sort on
     */
    private $sortType = self::SORT_TYPE_AVERAGE;

    /**
     * @var string direction to sort
     */
    private $sortDirection = self::SORT_DIR_ASC;

    /**
     * CalculationService constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->setLogger($logger);
    }

    /**
     * @param array $domainObject
     * @return sortingService
     */
    public function setDomainObjects(array $domainObject):SortingService
    {
        $this->domainObjects = $domainObject;
        return $this;
    }

    /**
     * sorts the domain objects
     *
     * @param string $direction
     * @return SortingService
     */
    public function sort(string $direction = self::SORT_DIR_ASC, string $sortType = self::SORT_TYPE_AVERAGE):SortingService
    {
        $this->sortType = $sortType;
        $this->sortDirection = $direction;
        $callback = ($direction == self::SORT_DIR_DESC) ? 'sortComparisonDesc' : 'sortComparisonAsc';

        $this->logger->info(sprintf('sorting domain objects by %s %s', $sortType, $direction));
        usort($this->domainObjects, [$this, 'sortComparison']);
        return $this;
    }

    /**
     * comparison function used for sorting the domain objects
     *
     * @param DomainObjectInterface $a
     * @param DomainObjectInterface $b
     * @return int
     */
    public function sortComparison(DomainObjectInterface $a, DomainObjectInterface $b)
    {
        switch($this->sortType) {
            case self::SORT_TYPE_MAX:
                $a = $a->findMax();
                $b = $b->findMax();
                break;
            case self::SORT_TYPE_MIN:
                $a = $a->findMin();
                $b = $b->findMin();
                break;
            case self::SORT_TYPE_AVERAGE:
            default:
                $a = $a->calculateAverage();
                $b = $b->calculateAverage();
                break;
        }
        $result = ($this->sortDirection == self::SORT_DIR_ASC) ? ( $a <=> $b ) : ( $b <=> $a );

        return $result;
    }


    /**
     * @return array
     */
    public function getDomainObjects():array
    {
        return $this->domainObjects;
    }

    /**
     * @return string
     */
    public function getSortType(): string
    {
        return $this->sortType;
    }

    /**
     * @param string $sortType
     */
    public function setSortType(string $sortType)
    {
        $this->sortType = $sortType;
    }

    /**
     * @return string
     */
    public function getSortDirection(): string
    {
        return $this->sortDirection;
    }

    /**
     * @param string $sortDirection
     */
    public function setSortDirection(string $sortDirection)
    {
        $this->sortDirection = $sortDirection;
    }
}