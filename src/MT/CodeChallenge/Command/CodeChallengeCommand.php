<?php
/**
 * User: John Brown
 * Date: 8/7/2017
 */
namespace MT\CodeChallenge\Command;

use MT\CodeChallenge\Domain\DomainObjectInterface;
use MT\CodeChallenge\Exception\FileOpenException;
use MT\CodeChallenge\Service\sortingService;
use MT\CodeChallenge\Service\InputParserService;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

/**
 * Class CodeChallengeCommand
 * @package MT
 * @subpackage CodeChallenge\Command
 */
class CodeChallengeCommand extends Command
{
    use LoggerAwareTrait;
    const INCLUDE_NONE_NAME = 'include_none';
    const FILE_INPUT_NAME = 'input_file';
    const DIRECTORY_INPUT_NAME = 'directory';
    const SORT_DIRECTION_NAME = 'sorting_direction';
    const SORT_TYPE_NAME = 'sorting_type';

    /**
     * @var sortingService
     */
    private $sortingService;

    /**
     * @var InputParserService
     */
    private $inputParser;

    /**
     * CodeChallengeCommand constructor.
     *
     * @param LoggerInterface    $logger
     * @param sortingService $sortingService
     * @param string|null        $name name of the command
     */
    public function __construct(LoggerInterface $logger, sortingService $sortingService, InputParserService $inputParser, $name = null)
    {
        parent::__construct($name);
        $this->setLogger($logger);

        $this->sortingService = $sortingService;
        $this->inputParser    = $inputParser;
    }

    /**
     * configure the command options
     */
    protected function configure()
    {
        $this->addArgument(self::FILE_INPUT_NAME, InputArgument::REQUIRED, "path to the file to be read")
            ->addOption(
                self::DIRECTORY_INPUT_NAME,
                'd',
                InputOption::VALUE_OPTIONAL,
                'directory to search for files in',
                realpath(__DIR__ . '/../../../../')
            )
            ->addOption(
                self::SORT_DIRECTION_NAME,
                null,
                InputOption::VALUE_OPTIONAL,
                sprintf('what direction to sort the values by (%s, %s)', SortingService::SORT_DIR_ASC, SortingService::SORT_DIR_DESC),
                SortingService::SORT_DIR_ASC
            )
            ->addOption(
                self::SORT_TYPE_NAME,
                null,
                InputOption::VALUE_OPTIONAL,
                sprintf('what attribute to sort the values by (%s, %s, %s)', SortingService::SORT_TYPE_AVERAGE, SortingService::SORT_TYPE_MIN, SortingService::SORT_TYPE_MAX),
                SortingService::SORT_TYPE_AVERAGE
            )
            ->addOption(
                self::INCLUDE_NONE_NAME,
                null,
                InputOption::VALUE_NONE,
                'Indicates if "None" should be treated as a 0'
            )
        ;
    }

    /**
     * execute the program
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fileData = null;
        $fileName = $input->getArgument(self::FILE_INPUT_NAME);
        $baseDir  = $input->getOption(self::DIRECTORY_INPUT_NAME);
        $sortDir  = $input->getOption(self::SORT_DIRECTION_NAME);
        $sortType  = $input->getOption(self::SORT_TYPE_NAME);
        $this->inputParser->includeNone($input->getOption(self::INCLUDE_NONE_NAME));

        try {
            $fileData = $this->inputParser->loadFile($baseDir, $fileName);
        } catch (FileOpenException $ex) {
            $output->writeln(sprintf('<error>%s: %s</error>', FileOpenException::class, $ex->getMessage()));
        } catch (FileNotFoundException $ex) {
            $output->writeln(sprintf('<error>%s: %s</error>', FileNotFoundException::class, $ex->getMessage()));
        }

        if ($fileData == null) return;

        $sorted = $this->sortingService->setDomainObjects($fileData)
            ->sort($sortDir, $sortType)
            ->getDomainObjects()
        ;

        $this->outputObjects($sorted, $output);
    }

    /**
     * outputObjects
     *
     * prints the results
     *
     * @param array $domainObjects
     * @param OutputInterface $output
     */
    private function outputObjects(array $domainObjects, OutputInterface $output)
    {
        $this->logger->info('Printing out objects');
        /** @var DomainObjectInterface $domainObject */
        foreach ($domainObjects as $domainObject) {
            $output->writeln(sprintf(
                '%s: Average: %.1f Max: %.1f Min: %.1f',
                $domainObject->getHost(),
                $domainObject->calculateAverage(),
                $domainObject->findMax(),
                $domainObject->findMin()
            ));
        }
    }
}
