<?php
/**
 * User: John Brown
 * Date: 8/7/2017
 */

namespace MT\CodeChallenge\Domain;

/**
 * Interface DomainObjectInterface
 * @package MT
 * @subpackage CodeChallenge\Domain
 */
interface DomainObjectInterface
{
    /**
     * @return mixed
     */
    public function calculateAverage();

    /**
     * @return mixed
     */
    public function findMax();

    /**
     * @return mixed
     */
    public function findMin();

    /**
     * @param array $data
     * @return mixed
     */
    public function setData(array $data):DomainObjectInterface;

    /**
     * @return array
     */
    public function getData():array;

    /**
     * @return string
     */
    public function getHost(): string;

    /**
     * @param string $host
     * @return DomainObjectInterface
     */
    public function setHost(string $host):DomainObjectInterface;
}