<?php
/**
 * User: John Brown
 * Date: 8/7/2017
 */
namespace MT\CodeChallenge\Domain;

/**
 * Class DomainObject
 *
 * Obect for accessing the values within the input file
 *
 * @package MT
 * @subpackage CodeChallenge\Domain
 */
class DomainObject implements DomainObjectInterface
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var \DateTime
     */
    private $startTime;

    /**
     * @var \DateTime
     */
    private $endTime;

    /**
     * Note: This value is technically an unknown, based on contexte I have called it timeframe but if 
     *       it is to be used please verify this is correct
     * @var int
     */
    private $timeframe;

    /**
     * @var float[]
     */
    private $data;

    /**
     * @var bool
     */
    private $caluclatedValues = [];

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @param string $host
     * @return DomainObjectInterface
     */
    public function setHost(string $host):DomainObjectInterface
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartTime(): \DateTime
    {
        return $this->startTime;
    }

    /**
     * @param \DateTime $startTime
     * @return DomainObject
     */
    public function setStartTime(\DateTime $startTime):DomainObject
    {
        $this->startTime = $startTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndTime(): \DateTime
    {
        return $this->endTime;
    }

    /**
     * @param \DateTime $endTime
     * @return DomainObject
     */
    public function setEndTime(\DateTime $endTime):DomainObject
    {
        $this->endTime = $endTime;
        return $this;
    }

    /**
     * Note: This value is technically an unknown, based on contexte I have called it timeframe but if
     *       it is to be used please verify this is correct
     * @return int
     */
    public function getTimeframe(): int
    {
        return $this->timeframe;
    }

    /**
     * Note: This value is technically an unknown, based on contexte I have called it timeframe but if
     *       it is to be used please verify this is correct
     * @param int $timeframe
     * @return DomainObject
     */
    public function setTimeframe(int $timeframe):DomainObject
    {
        $this->timeframe = $timeframe;
        return $this;
    }

    /**
     * @return \float[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return DomainObjectInterface
     */
    public function setData(array $data):DomainObjectInterface
    {
        $this->data = $data;
        $this->caluclatedValues=[]; // clear calculated values as it is no longer valid
        return $this;
    }

    /**
     * @param $dataValue
     * @return DomainObjectInterface
     */
    public function addData($dataValue):DomainObjectInterface
    {
        $this->data[] = $dataValue;
        $this->caluclatedValues=[]; // clear calculated values as it is no longer valid
        return $this;
    }

    /**
     * findMin
     *
     * finds the min value in the data array
     *
     * @return float
     */
    public function findMin()
    {
        if(!isset($this->caluclatedValues['min'])) {
            $this->caluclatedValues['min'] = min($this->data);
        }
        return $this->caluclatedValues['min'];
    }

    /**
     * findMax
     *
     * finds the max value in the data array
     *
     * @return float
     */
    public function findMax()
    {
        if(!isset($this->caluclatedValues['max'])) {
            $this->caluclatedValues['max'] = max($this->data);
        }
        return $this->caluclatedValues['max'];
    }

    /**
     * calculates the average in the data array
     *
     * @return float
     */
    public function calculateAverage():float
    {
        if(!isset($this->caluclatedValues['average'])) {
            $this->caluclatedValues['average'] = (array_sum($this->data) / count($this->data));
        }
        return $this->caluclatedValues['average'];
    }
}
