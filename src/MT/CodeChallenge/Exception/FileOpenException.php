<?php
/**
 * User: John Brown
 * Date: 8/7/2017
 */

namespace MT\CodeChallenge\Exception;

/**
 * Class FileOpenException
 *
 * indicates that we could not open a file for reading
 * @package MT
 * @subpackage CodeChallenge\Exception
 */
class FileOpenException extends \Exception
{

}