<?php
/**
 * User: John Brown
 * Date: 8/7/2017
 */

namespace MT\CodeChallenge\Exception;

/**
 * Class InvalidInputException
 * @package MT
 * @subpackage CodeChallenge\Exception
 */
class InvalidInputException extends \Exception
{

}